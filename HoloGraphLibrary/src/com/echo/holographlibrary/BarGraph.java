/*
 *     Created by Daniel Nadeau
 *     daniel.nadeau01@gmail.com
 *     danielnadeau.blogspot.com
 * 
 *     Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.echo.holographlibrary;

import android.content.Context;
import android.graphics.*;
import android.graphics.Bitmap.Config;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class BarGraph extends View {

	private final static int DEFAULT_VALUE_FONT_SIZE = 30, AXIS_LABEL_FONT_SIZE = 15;
    private int mBubbleBackgroundColor;
    private int mBubbleTextColor;

    private ArrayList<Bar> mBars = new ArrayList<Bar>();
    private Paint mPaint = new Paint();
    private Rect mRectangle = null;
    private boolean mShowBarText = true;
    private boolean mShowAxis = true;
    private int mIndexSelected = -1;
    private OnBarClickedListener mListener;
    private Bitmap mFullImage;
    private boolean mShouldUpdate = false;
    private float mBubbleFontSize = DEFAULT_VALUE_FONT_SIZE;
    
    private Context mContext = null;
    private float dpi;
    private float mPaddingInBetween;
    private List<Bubble> mBubblesToDraw = new ArrayList<Bubble>();

    public BarGraph(Context context) {
        super(context);
        init(context);
    }

    public BarGraph(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        dpi = mContext.getResources().getDisplayMetrics().density;
        mPaddingInBetween = 8 * dpi;
        mBubbleFontSize = DEFAULT_VALUE_FONT_SIZE * mContext.getResources().getDisplayMetrics().scaledDensity;
        mBubbleTextColor = Color.WHITE;
        mBubbleBackgroundColor = Color.RED;
    }
    
    public void setShowBarText(boolean show){
        mShowBarText = show;
    }

    public void setBubbleFontSize(float bubbleFontSize) {
        mBubbleFontSize = bubbleFontSize;
    }

    public int getBubbleBackgroundColor() {
        return mBubbleBackgroundColor;
    }

    public void setBubbleBackgroundColor(int bubbleBackgroundColor) {
        mBubbleBackgroundColor = bubbleBackgroundColor;
    }

    public int getBubbleTextColor() {
        return mBubbleTextColor;
    }

    public void setBubbleTextColor(int bubbleTextColor) {
        mBubbleTextColor = bubbleTextColor;
    }

    public void setPaddingInBetween(float paddingInBetween) {
        mPaddingInBetween = paddingInBetween;
    }

    public void setShowAxis(boolean show){
        mShouldUpdate = true;
        mShowAxis = show;
        postInvalidate();
    }
    
    public void setBars(ArrayList<Bar> points){
        this.mBars = points;
        mShouldUpdate = true;
        postInvalidate();
    }
    
    public ArrayList<Bar> getBars(){
        return this.mBars;
    }

    public void onDraw(Canvas ca) {
        if (mFullImage == null || mShouldUpdate) {
            mFullImage = Bitmap.createBitmap(getWidth(), getHeight(), Config.ARGB_8888);
            Canvas canvas = new Canvas(mFullImage);
            canvas.drawColor(Color.TRANSPARENT);
            mPaint.setAntiAlias(true);

            float maxValue = 0f;
            int selectPadding = (int) (4 * dpi);
            float bottomPadding = getPaddingBottom();
            if (mShowAxis) {
                bottomPadding += 30 * dpi;
            }
            
            float usableHeight;
            if (mShowBarText) {
                this.mPaint.setTextSize(mBubbleFontSize);
                Rect r3 = new Rect();
                this.mPaint.getTextBounds("$", 0, 1, r3);
                usableHeight = getHeight()-bottomPadding-Math.abs(r3.top-r3.bottom)-24 * dpi;
            } else {
                usableHeight = getHeight()-bottomPadding;
            }
             
            // Draw x-axis line
            if (mShowAxis){
                mPaint.setColor(Color.BLACK);
                mPaint.setStrokeWidth(2 * dpi);
                mPaint.setAlpha(50);
                canvas.drawLine(getPaddingLeft(), getHeight()-bottomPadding+10* dpi, getWidth()-getPaddingRight(), getHeight()-bottomPadding+10* dpi, mPaint);
            }
            float barWidth = (getWidth() - mPaddingInBetween*(mBars.size()-1) - getPaddingLeft() - getPaddingRight())/mBars.size();

            // Maximum y value = sum of all values.
            for (final Bar bar : mBars) {
                if (bar.getValue() > maxValue) {
                    maxValue = bar.getValue();
                }
            }
            
            mRectangle = new Rect();
            mBubblesToDraw.clear();

            int count = 0;
            for (final Bar bar : mBars) {
                // Set bar bounds
                int left = (int)(getPaddingLeft() + mPaddingInBetween*count + barWidth*count);
                //Avoid NaN in bar height calculation in case of empty data set;
                float barHeight = maxValue == 0f ? 0 : usableHeight * (bar.getValue() / maxValue);
                int top = (int) (getHeight() - bottomPadding - barHeight);
                int right = (int)(getPaddingLeft() + mPaddingInBetween*count + barWidth*(count+1));
                int bottom = (int)(getHeight()-bottomPadding);
                mRectangle.set(left, top, right, bottom);

                // Draw bar
                this.mPaint.setColor(bar.getColor());
                canvas.drawRect(mRectangle, this.mPaint);

                // Create selection region
                Path path = new Path();
                path.addRect(new RectF(mRectangle.left-selectPadding, mRectangle.top-selectPadding, mRectangle.right+selectPadding, mRectangle.bottom+selectPadding), Path.Direction.CW);
                bar.setPath(path);
                bar.setRegion(new Region(mRectangle.left-selectPadding, mRectangle.top-selectPadding, mRectangle.right+selectPadding, mRectangle.bottom+selectPadding));

                // Draw x-axis label text
                if (mShowAxis){
                    this.mPaint.setTextSize(AXIS_LABEL_FONT_SIZE * mContext.getResources().getDisplayMetrics().scaledDensity);
                    int x = (int)(((mRectangle.left+mRectangle.right)/2)-(this.mPaint.measureText(bar.getName())/2));
                    int y = (int) (getHeight()- getPaddingBottom() - 3 * mContext.getResources().getDisplayMetrics().scaledDensity);
                    canvas.drawText(bar.getName(), x, y, this.mPaint);
                }

                if (mIndexSelected == count && mListener != null) {
                    this.mPaint.setColor(Color.parseColor("#33B5E5"));
                    this.mPaint.setAlpha(100);
                    canvas.drawPath(bar.getPath(), this.mPaint);
                    this.mPaint.setAlpha(255);
                }

                //Postpone draw bubbles to draw them on top of other bars if overlaps
                if (bar.isBubbleVisible()) {
                    Bubble bubble = new Bubble(mRectangle, bar);
                    mBubblesToDraw.add(bubble);
                }
                count++;
            }

            // Draw bubbles if needed
            if (mShowBarText) {
                for (Bubble bubble : mBubblesToDraw) {
                    drawValueText(mPaint, canvas, bubble.mBar, bubble.mRectangle);
                }
            }
            mBubblesToDraw.clear();

            mShouldUpdate = false;
        }
        
        ca.drawBitmap(mFullImage, 0, 0, null);
        
    }

    private void drawValueText(Paint paint, Canvas canvas, Bar bar, Rect barRect) {
        paint.setTextSize(mBubbleFontSize);
        paint.setColor(mBubbleTextColor);
        Rect r2 = new Rect();
        paint.getTextBounds(bar.getValueString(), 0, 1, r2);

        float textWidth = paint.measureText(bar.getValueString());
        int arrowHeight = (int) (mBubbleFontSize / 3);
        int boundLeft = (int) (((barRect.left + barRect.right) / 2) - (textWidth / 2) - 10 * dpi);
        int boundRight = (int) (((barRect.left + barRect.right) / 2) + (textWidth / 2) + 10 * dpi);
        //fix left/right bounds if they are outside drawable area
        int moveX = 0;
        int outsideLeft = boundLeft - getPaddingLeft();
        if (outsideLeft < 0) {
            //Move bounds right
            moveX = -outsideLeft;
        }
        int outsideRight = (getWidth() - getPaddingRight()) - boundRight;
        if (outsideRight < 0) {
            moveX = outsideRight;
        }
        boundLeft += moveX;
        boundRight += moveX;

        int boundBottom = barRect.top - arrowHeight;
        int boundTop = (int) (boundBottom + (r2.top - r2.bottom) - 18 * dpi);
        int barCenterX = (barRect.left + barRect.right) / 2;

        Paint arrowPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        arrowPaint.setColor(mBubbleBackgroundColor);
        arrowPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        arrowPaint.setAntiAlias(true);

        Path arrowPath = new Path();
        arrowPath.setFillType(Path.FillType.EVEN_ODD);
        arrowPath.moveTo(barCenterX, barRect.top);
        arrowPath.lineTo(barCenterX + (arrowHeight / 2), barRect.top - arrowHeight);
        arrowPath.lineTo(barCenterX - (arrowHeight / 2), barRect.top - arrowHeight);
        arrowPath.close();
        arrowPath.addRect(boundLeft, boundTop, boundRight, boundBottom, Path.Direction.CW);
        canvas.drawPath(arrowPath, arrowPaint);

        float x = barCenterX - (textWidth / 2) + moveX;
        float y = barRect.top - (barRect.top - boundTop) / 2f + (float) Math.abs(r2.top - r2.bottom) / 2f * 0.7f;
        canvas.drawText(bar.getValueString(), x, y, paint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (isClickable()){
            return super.onTouchEvent(event);
        }

        Point point = new Point();
        point.x = (int) event.getX();
        point.y = (int) event.getY();
        
        int count = 0;
        for (Bar bar : mBars){
            Region r = new Region();
            final Path barPath = bar.getPath();
            final Region barRegion = bar.getRegion();
            if (barPath != null && barRegion != null) {
                r.setPath(barPath, barRegion);
            }
            if (r.contains(point.x, point.y) && event.getAction() == MotionEvent.ACTION_DOWN){
                mIndexSelected = count;
            } else if (event.getAction() == MotionEvent.ACTION_UP){
                if (r.contains(point.x, point.y) && mListener != null){
                    if (mIndexSelected > -1) mListener.onClick(mIndexSelected);
                    mIndexSelected = -1;
                }
            }
            else if(event.getAction() == MotionEvent.ACTION_CANCEL)
            	mIndexSelected = -1;
            
            count++;
        }
        
        if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL){
            mShouldUpdate = true;
            postInvalidate();
        }

        return true;
    }
    
    @Override
    protected void onDetachedFromWindow()
    {
    	if(mFullImage != null)
    		mFullImage.recycle();
    	
    	super.onDetachedFromWindow();
    }
    
    public void setOnBarClickedListener(OnBarClickedListener listener) {
        this.mListener = listener;
    }

    public void hideAllBubbles() {
        for (Bar bar : getBars()) {
            bar.setBubbleHidden();
        }
        postInvalidate();
    }

    public void showBubble(int index) {
        Bar bar = getBars().get(index);
        bar.setBubbleVisible();
        postInvalidate();
    }

    public void hideBubble(int index) {
        Bar bar = getBars().get(index);
        bar.setBubbleHidden();
        postInvalidate();
    }

    public boolean isBubbleShown(int index) {
        Bar bar = getBars().get(index);
        return bar.isBubbleVisible();
    }

    public interface OnBarClickedListener {
        abstract void onClick(int index);
    }

    private class Bubble {

        private final Rect mRectangle;
        private final Bar mBar;

        public Bubble(Rect rectangle, Bar bar) {
            mRectangle = new Rect(rectangle);
            mBar = bar;
        }
    }
}
