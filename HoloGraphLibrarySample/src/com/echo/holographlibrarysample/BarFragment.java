/*
 * 	   Created by Daniel Nadeau
 * 	   daniel.nadeau01@gmail.com
 * 	   danielnadeau.blogspot.com
 * 
 * 	   Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.echo.holographlibrarysample;

import java.util.ArrayList;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import com.actionbarsherlock.app.SherlockFragment;
import com.echo.holographlibrary.Bar;
import com.echo.holographlibrary.BarGraph;
import com.echo.holographlibrary.BarGraph.OnBarClickedListener;

public class BarFragment extends SherlockFragment {

    private BarGraph barGraph;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View v = inflater.inflate(R.layout.fragment_bargraph, container, false);
		ArrayList<Bar> points = new ArrayList<Bar>();
		Bar d = new Bar();
		d.setColor(Color.parseColor("#99CC00"));
		d.setName("Test1");
		d.setValue(1000);
		d.setValueString("$1,000");
		Bar d2 = new Bar();
		d2.setColor(Color.parseColor("#FFBB33"));
		d2.setName("Test2");
		d2.setValue(2000);
		d2.setValueString("$2,000");
        d2.setBubbleVisible();
		points.add(d);
		points.add(d2);
		
	    barGraph = (BarGraph)v.findViewById(R.id.bargraph);
        barGraph.setPaddingInBetween(0);
        barGraph.setBars(points);

		barGraph.setOnBarClickedListener(new OnBarClickedListener() {

            @Override
            public void onClick(int index) {

            }

        });

        Button btnShowAxis = (Button) v.findViewById(R.id.btnShowAxis);
        btnShowAxis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                barGraph.setShowAxis(true);
            }
        });

        Button btnHideAxis = (Button) v.findViewById(R.id.btnHideAxis);
        btnHideAxis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                barGraph.setShowAxis(false);
            }
        });

        return v;
	}
}
